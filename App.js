/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from "react";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Profile,
  ImageBackground
} from "react-native";

import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator,
  createDrawerNavigator,
  DrawerActions,
  DrawerItems
} from "react-navigation";
import PatientEntryScreen from "./src/screens/PatientEntryScreen";
import HospitalEntryScreen from "./src/screens/hospitalEntryScreen";
import DashboardScreen from "./src/screens/dashboard";
import UserLoginScreen from "./src/screens/userLogin";
import PatientDetailsScreen from "./src/screens/patientDetailsScreen";
import HospitalDetailsScreen from "./src/screens/HospitalDetailsScreen";
import UserDashboardScreen from "./src/screens/userDashboardScreen";
import NewHospitalListScreen from "./src/screens/HospitalListScreen";
import newLoginScreen from "./src/screens/LoginScreen";
import NewPatientList from "./src/screens/PatientListScreen";
import NewHomeScreen from "./src/screens/HomeScreen";
import { TouchableOpacity } from "react-native-gesture-handler";
import SideMenu from "./src/screens/sidemenu";
import UserDrawer from "./src/screens/userDrawer";

export const AuthStack = createStackNavigator(
  {
    HomeScreen: NewHomeScreen
  },
  {
    headerMode: null
  }
);

// const customDrawer=props=>(
//   <View style={{height:"100%"}}>
//     {/* <DrawerItems {...props}/> */}
//              <View style={{height:100, backgroundColor:"#7112B8",flexDirection:"row"}}>
//                  <View style={{borderRadius:60,marginTop:20,marginLeft:10 ,height:70,width:"25%", backgroundColor:"white"}}>

//                  </View>
//                  <View style={{margin:20,marginTop:30}}>
//                      <Text style={{color:"white",fontSize:20,fontWeight:"bold"}}>Admin Name</Text>
//                      <Text style={{color:"white",fontSize:15}}>details about admin</Text>

//                  </View>
//              </View>
//              <View style={{height:50, backgroundColor:"#046DAD40",marginTop:5,flexDirection:"column",justifyContent:"center",paddingLeft:15}}>
//              <TouchableOpacity onPress={() =>this.props.navigation.navigate("Dashboard")}>
//              <Text style={{color:"black", fontSize:20}}>Dashboard</Text>
//              </TouchableOpacity>
//              </View>

//              <View style={{height:50, backgroundColor:"#046DAD40",marginTop:5,flexDirection:"column",justifyContent:"center",paddingLeft:15}}>
//              <TouchableOpacity onPress={() =>this.props.navigation.navigate("HospitalEntryScreen")}>
//              <Text style={{color:"black", fontSize:20}}>Hospital Entry</Text>
//              </TouchableOpacity>
//              </View>

//              <View style={{height:50, backgroundColor:"#046DAD40",marginTop:5,flexDirection:"column",justifyContent:"center",paddingLeft:15}}>
//              <TouchableOpacity onPress={() =>this.props.navigation.navigate("HospitalList")}>
//              <Text style={{color:"black", fontSize:20}}>Hospital List</Text>
//              </TouchableOpacity>
//              </View>

//              <View style={{height:50, backgroundColor:"#046DAD40",marginTop:5,flexDirection:"column",justifyContent:"center",paddingLeft:15}}>
//              <TouchableOpacity onPress={() =>this.props.navigation.navigate("Logout")}>
//              <Text style={{color:"black", fontSize:20}}>Log Out</Text>
//              </TouchableOpacity>
//              </View>

//          </View>
// );

export const AppDrawerNav = createDrawerNavigator(
  {
    Dashboard: DashboardScreen,
    HospitalEntryScreen: HospitalEntryScreen,
    HospitalList: NewHospitalListScreen,
    HospitalDetails: HospitalDetailsScreen
  },
  {
    contentComponent: props => <SideMenu {...props} />
    //contentComponent:customDrawer
    //contentComponent:props=><customDrawer{...props}/>
  },
  {
    getCustomActionCreators: (route, statekey) => {
      return {
        toggleLeftDrawer: () => DrawerActions.toggleDrawer({ key: statekey })
      };
    },
    drawerPosition: "left"
  },

  {
    headerMode: null
  }
);
export const UserDrawerNav = createDrawerNavigator(
  {
    UserDashboard: UserDashboardScreen,
    PatientEntry: PatientEntryScreen,
    PatientList: NewPatientList,
    PatientDetails: PatientDetailsScreen
    //"Patient Entry": PatientEntryScreen,
    //"Patient List": NewPatientList,
    //"Log out": NewHomeScreen
  },
  {
    contentComponent: props => <UserDrawer {...props} />
  },

  {
    getCustomActionCreators: (route, statekey) => {
      return {
        toggleLeftDrawer: () => DrawerActions.toggleDrawer({ key: statekey })
      };
    },
    drawerPosition: "left"
  },
  {
    headerMode: null
  }
);
export const StackNav = createStackNavigator(
  {
    Login: newLoginScreen,
    UserLogin: UserLoginScreen,
    Drawer: AppDrawerNav,
    UserDrawer: UserDrawerNav,
    HospitalEntryScreen: HospitalEntryScreen,
    DashboardScreen: DashboardScreen,
    PatientEntry: PatientEntryScreen,
    PatientList: NewPatientList,
    PatientDetails: PatientDetailsScreen,
    HospitalDetails: HospitalDetailsScreen,
    HospitalList: NewHospitalListScreen,
    Logout: NewHomeScreen
  },
  {
    header: null,
    headerMode: "none"
  }
);

const RootStack = createStackNavigator(
  {
    Auth: AuthStack,
    App: StackNav
    // Test: Test
  },
  { initialRouteName: "Auth", header: null, headerMode: "none" }
);
const AppContainer = createAppContainer(RootStack);
const App = () => {
  return <AppContainer />;
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "black"
  },
  engine: {
    // position: "absolute",
    // right: 0
  },
  body: {
    backgroundColor: "#3F8585",
    alignItems: "center"
  }
});

export default App;
