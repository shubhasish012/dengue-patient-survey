import React, { Component, Fragment } from "react";
import { Input, Icon, Button } from "react-native-elements";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Alert,
  ImageBackground
} from "react-native";
import HeaderComponent from "../components/HeaderComponent";
export default class PatientDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  static navigationOptions = {
    header: null
  };

  render() {
    const { navigation } = this.props;
    const user_name = navigation.getParam("userName", "NO-User");
    const userId = navigation.getParam("userId");
    const fatherName = navigation.getParam("fatherName");
    const motherName = navigation.getParam("motherName");
    const mobileNumber = navigation.getParam("mobileNumber");
    const EmergencyContactName = navigation.getParam("EmergencyContactName");
    const EmergencyMobileNumber = navigation.getParam("EmergencyMobileNumber");
    const Address = navigation.getParam("Address");
    const Age = navigation.getParam("Age");
    const BloodGroup = navigation.getParam("BloodGroup");
    const date = navigation.getParam("Date");
    return (
      <View>
        <StatusBar barStyle="light-content" backgroundColor="#046DAD" />
        <HeaderComponent
          title="Patient details"
          leftButton={() => this.props.navigation.openDrawer()}
          iconTitle="menu"
        />
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={require("../images/Asset2.png")}
        >
          <ScrollView>
            <View style={styles.body}>
              <Text style={styles.title}>{user_name}'s Information</Text>
              <Text style={styles.text}>Id: {userId}</Text>
              <Text style={styles.text}>Name: {user_name}</Text>
              <Text style={styles.text}>Father: {fatherName}</Text>
              <Text style={styles.text}>Mother: {motherName}</Text>
              <Text style={styles.text}>Mobile: {mobileNumber}</Text>
              <Text style={styles.text}>
                Emargency Contact Name: {EmergencyContactName}
              </Text>
              <Text style={styles.text}>
                Emergency Contact: {EmergencyMobileNumber}
              </Text>
              <Text style={styles.text}>Address: {Address}</Text>
              <Text style={styles.text}>Age: {Age}</Text>
              <Text style={styles.text}>Blood Group: {BloodGroup}</Text>
              <Text style={styles.text}>Date: {date}</Text>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    color: "#00789F",
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 30
  },
  body: {
    alignItems: "center",
    backgroundColor: "#FFFFFF90",
    opacity: 50,
    margin: 15,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  text: {
    fontSize: 20,
    color: "#00789F",
    margin: 10,
    fontFamily: "Montserrat",
    fontStyle: "italic",
    fontWeight: "bold"
  }
});
