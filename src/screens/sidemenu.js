import React, { Component } from "react";
import {Thumbnail} from "native-base";
import { ImageBackground,StyleSheet,Image,View,Text ,TouchableOpacity,ScrollView  } from "react-native";

export default class SideMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          UserName: "Dhaka Medical College",
          UserDetails: "Dhaka, Bangladesh.",
          active: false
        };
      }
    render() {
      return (
         <View style={{height:"100%"}}>
             <View style={styles.header}>
                 <View style={styles.image}>
                 <Thumbnail
                              circle
                              source={require("../images/Asset1.png")}
                            />
               

                 </View>
                 <View style={{marginLeft:5,marginTop:25}}>
                     <Text style={{color:"white",fontSize:20,fontWeight:"bold"}}>{this.state.UserName}</Text>
                     <Text style={{color:"white",fontSize:15}}>{this.state.UserDetails}</Text>

                 </View>
             </View>
             <View style={styles.menuItem}>
             <TouchableOpacity onPress={() =>this.props.navigation.navigate("Dashboard")}>
             <Text style={styles.menuText}>Dashboard</Text>
             </TouchableOpacity>
             </View>

             <View style={styles.menuItem}>
             <TouchableOpacity onPress={() =>this.props.navigation.navigate("HospitalEntryScreen")}>
             <Text style={styles.menuText}>Hospital Entry</Text>
             </TouchableOpacity>
             </View>

             <View style={styles.menuItem}>
             <TouchableOpacity onPress={() =>this.props.navigation.navigate("HospitalList")}>
             <Text style={styles.menuText}>Hospital List</Text>
             </TouchableOpacity>
             </View>

             <View style={styles.menuItem}>
             <TouchableOpacity onPress={() =>this.props.navigation.navigate("Logout")}>
             <Text style={styles.menuText}>Log Out</Text>
             </TouchableOpacity>
             </View>

            
         </View>
      );
    }
  }
  const styles={
    header:{
      height:100,
      backgroundColor:"#7112B8",
      flexDirection:"row",
      paddingLeft:5


    },
    image:{
        flexDirection:"column",
        justifyContent:"center",
      
      },
      menuItem:{
          height:40, 
          backgroundColor:"#7112B830",
          marginTop:3,
          flexDirection:"column",
          justifyContent:"center",
          paddingLeft:15
      },
      menuText:{
          color:"black", 
          fontSize:20}
}
 