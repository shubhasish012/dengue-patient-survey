import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Fab,
  View,
  StyleSheet
} from "native-base";
import { ImageBackground, TouchableOpacity, ScrollView } from "react-native";
import HeaderComponent from "../components/HeaderComponent";
//import HospitalList from "../components/HospitalList";
import PropTypes from "prop-types";
import { Tooltip } from "react-native-elements";

import { db } from "../config";
//import { ScrollView } from "react-native-gesture-handler";

let HospitalsRef = db.ref("/hopitals");

export default class NewHospitalListScreen extends Component {
  static propTypes = {
    Hospitals: PropTypes.array.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      Hospitals: [],
      active: false
    };
  }
  componentDidMount() {
    HospitalsRef.on("value", snapshot => {
      let data = snapshot.val();
      let Hospitals = Object.values(data);
      this.setState({ Hospitals });
    });
  }

  render() {
    return (
      <View>
        {/* <Header /> */}
        <HeaderComponent
          title="Hospital List"
          leftButton={() => this.props.navigation.openDrawer()}
          iconTitle="menu"
        />

        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={require("../images/Asset2.png")}
        >
          <ScrollView>
            <View>
              {this.state.Hospitals.length > 0 ? (
                <Container>
                  <Content>
                    <List>
                      {this.state.Hospitals.map((hospital, index) => {
                        return (
                          <ListItem thumbnail style={styles.itemContain}>
                            <Left>
                              <Thumbnail
                                square
                                source={require("../images/Asset1.png")}
                              />
                            </Left>
                            <Body>
                              <TouchableOpacity
                                onPress={() =>
                                  this.props.navigation.navigate(
                                    "HospitalDetails",
                                    {
                                      HospitalName: hospital.HospitalName,
                                      Licensenumber: hospital.Licensenumber,
                                      Address: hospital.Address,
                                      Email: hospital.Email,
                                      Existing: hospital.Existing,
                                      Released: hospital.Release,
                                      dead: hospital.Dead
                                    }
                                  )
                                }
                              >
                                <Text>{hospital.HospitalName}</Text>
                                <View
                                  style={{
                                    flexDirection: "row",
                                    justifyContent: "flex-start"
                                  }}
                                >
                                  <Text
                                    style={{
                                      fontSize: 14,
                                      marginRight: 8,
                                      color: "#0D649F"
                                    }}
                                  >
                                    Existing: {hospital.Existing}
                                  </Text>
                                  <Text
                                    style={{
                                      fontSize: 14,
                                      marginRight: 8,
                                      color: "#0C8749"
                                    }}
                                  >
                                    Released: {hospital.Release}
                                  </Text>
                                  <Text
                                    style={{
                                      fontSize: 14,
                                      marginRight: 8,
                                      color: "#C20000"
                                    }}
                                  >
                                    Dead: {hospital.Dead}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </Body>
                          </ListItem>
                        );
                      })}
                    </List>
                  </Content>
                </Container>
              ) : (
                <Text>No Hospitals</Text>
              )}
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const styles = {
  itemContain: {
    backgroundColor: "#fff",
    marginTop: 10,
    marginRight: 10,
    paddingLeft: 8,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    opacity: 0.95
  }
};
