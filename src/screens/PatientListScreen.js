import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Fab,
  View,
  Tab,
  Tabs
} from "native-base";
import ExistingList from "../components/ExistingList";
import ReleasedList from "../components/ReleasedList";
import DeadList from "../components/DeadList";
import HeaderComponent from "../components/HeaderComponent";
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Alert,
  ImageBackground
} from "react-native";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";
import PropTypes from "prop-types";
import { db } from "../config";

let ExistingPatientsRef = db.ref(
  "/hopitals/-LlkzqZ0TZUjViIu2bCU/ExistingPatient"
);

let ReleasedPatientsRef = db.ref(
  "/hopitals/-LlkzqZ0TZUjViIu2bCU/ReleasedPatient"
);
let DeadPatientsRef = db.ref("/hopitals/-LlkzqZ0TZUjViIu2bCU/DeadPatient");
// let PatientsRef = db
//   .child("patient")
//   .orderByChild("Status")
//   .equalTo("Existing");
export default class NewPatientList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Patients: [],
      Released: [],
      Dead: [],
      active: false
    };
  }
  componentDidMount() {
    ExistingPatientsRef.on("value", snapshot => {
      let data = snapshot.val();
      let Patients = Object.values(data);
      this.setState({ Patients });
    });
    ReleasedPatientsRef.on("value", snapshot => {
      let data = snapshot.val();
      let Released = Object.values(data);
      this.setState({ Released });
    });
    DeadPatientsRef.on("value", snapshot => {
      let data = snapshot.val();
      let Dead = Object.values(data);
      this.setState({ Dead });
    });
  }
  static propTypes = {
    Patients: PropTypes.array.isRequired
  };
  // _menu = null;

  // setMenuRef = ref => {
  //   this._menu = ref;
  // };
  // hideMenu = () => {
  //   this._menu.hide();
  // };
  // hideDialog = () => {
  //   this.setState({ visible: false, dialogVisible: false });
  // };

  // showMenu = () => {
  //   this._menu.show();
  // };
  ReleasedData = data => {
    var id = "/hopitals/-LlkzqZ0TZUjViIu2bCU/ReleasedPatient/" + data.PatientId;
    db.ref(id).set({
      PatientId: data.PatientId,
      PatientName: data.PatientName,
      FathersName: data.FathersName,
      MothersName: data.MothersName,
      MobileNumber: data.MobileNumber,
      EmergencyContactName: data.EmergencyContactName,
      EmergencyMobileNumber: data.EmergencyMobileNumber,
      Address: data.Address,
      Age: data.Age,
      BloodGroup: data.BloodGroup,
      Date: data.Date
    });

    var ide =
      "/hopitals/-LlkzqZ0TZUjViIu2bCU/ExistingPatient/" + data.PatientId;
    db.ref(ide).remove();
  };

  DeadData = data => {
    var id = "/hopitals/-LlkzqZ0TZUjViIu2bCU/DeadPatient/" + data.PatientId;
    db.ref(id).set({
      PatientId: data.PatientId,
      PatientName: data.PatientName,
      FathersName: data.FathersName,
      MothersName: data.MothersName,
      MobileNumber: data.MobileNumber,
      EmergencyContactName: data.EmergencyContactName,
      EmergencyMobileNumber: data.EmergencyMobileNumber,
      Address: data.Address,
      Age: data.Age,
      BloodGroup: data.BloodGroup,
      Date: data.Date
    });

    var ide =
      "/hopitals/-LlkzqZ0TZUjViIu2bCU/ExistingPatient/" + data.PatientId;
    db.ref(ide).remove();
  };

  text = data => {
    Alert.alert(data.PatientName);
  };
  render() {
    return (
      <Container>
        <HeaderComponent
          title="Patient List"
          leftButton={() => this.props.navigation.openDrawer()}
          iconTitle="menu"
        />

        <Tabs>
          <Tab
            tabStyle={{ backgroundColor: "#046DAD" }}
            textStyle={{ color: "#fff" }}
            activeTabStyle={{ backgroundColor: "#046DAD" }}
            activeTextStyle={{ color: "#fff", fontWeight: "bold" }}
            heading="Existing"
          >
            {this.state.Patients.length > 0 ? (
              // <ExistingList Patients={this.state.Patients} />
              <ImageBackground
                style={{ width: "100%", height: "100%" }}
                source={require("../images/Asset1.png")}
              >
                <Content>
                  <List>
                    {this.state.Patients.map((Patient, index) => {
                      return (
                        <ListItem thumbnail style={styles.itemContain}>
                          <Left>
                            {/* source={{ uri: "../images/Asset1.png" }} */}
                            <Thumbnail
                              circle
                              source={require("../images/Asset1.png")}
                            />
                          </Left>
                          <Body>
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate(
                                  "PatientDetails",
                                  {
                                    userId: Patient.PatientId,
                                    userName: Patient.PatientName,
                                    fatherName: Patient.FathersName,
                                    motherName: Patient.MothersName,
                                    mobileNumber: Patient.MobileNumber,
                                    EmergencyContactName:
                                      Patient.EmergencyContactName,
                                    EmergencyMobileNumber:
                                      Patient.EmergencyMobileNumber,
                                    Address: Patient.Address,
                                    Age: Patient.Age,
                                    BloodGroup: Patient.BloodGroup,
                                    Date: Patient.Date
                                  }
                                )
                              }
                            >
                              <Text>{Patient.PatientId}</Text>
                              <Text>{Patient.PatientName}</Text>
                              <Text>Father Name: {Patient.FathersName}</Text>
                            </TouchableOpacity>
                          </Body>
                          <Right>
                            <View style={{ justifyContent: "center" }}>
                              <Button
                                small
                                bordered
                                rounded
                                info
                                style={{ marginBottom: 5, padding: 0 }}
                                onPress={() => this.ReleasedData(Patient)}
                                // onPress={() => this.text(Patient)}
                              >
                                <Text
                                  style={{ fontSize: 10, alignItems: "center" }}
                                >
                                  Release
                                </Text>
                              </Button>
                              <Button
                                small
                                bordered
                                rounded
                                danger
                                style={{
                                  marginBottom: 5,
                                  justifyContent: "center",
                                  padding: 0
                                }}
                                onPress={() => this.DeadData(Patient)}
                              >
                                <Text
                                  style={{
                                    fontSize: 10
                                  }}
                                >
                                  Dead
                                </Text>
                              </Button>
                            </View>

                            {/* <Menu
                              ref={this.setMenuRef}
                              button={
                                <Icon
                                  onPress={this.showMenu}
                                  type="FontAwesome"
                                  name="ellipsis-v"
                                  style={{
                                    padding: 10
                                  }}
                                />
                              }
                            >
                              <MenuItem onPress={this.ReleasedData(Patient)}>
                                Release
                              </MenuItem>
                              <MenuDivider />
                              <MenuItem onPress={this.hideMenu}>Dead</MenuItem>
                              <MenuDivider />
                              <MenuItem onPress={this.hideMenu}>
                                Details
                              </MenuItem>
                            </Menu> */}
                          </Right>
                        </ListItem>
                      );
                    })}
                  </List>

                  {/* ========= */}
                  {/* <View style={styles.container}>
              <Dialog
                visible={this.state.visible}
                footer={
                  <DialogFooter>
                    <DialogButton text="CANCEL" onPress={() => {}} />
                    <DialogButton text="OK" onPress={() => {}} />
                  </DialogFooter>
                }
              >
                <DialogContent>
                  <Text>tt</Text>
                </DialogContent>
              </Dialog>
            </View> */}
                  {/* ======== */}
                </Content>
              </ImageBackground>
            ) : (
              <Text>No Patients</Text>
            )}
          </Tab>
          <Tab
            heading="Released"
            tabStyle={{ backgroundColor: "#046DAD" }}
            textStyle={{ color: "#fff" }}
            activeTabStyle={{ backgroundColor: "#046DAD" }}
            activeTextStyle={{ color: "#fff", fontWeight: "bold" }}
          >
            {/* {this.state.Released.length > 0 ? (
              <ReleasedList Released={this.state.Released} />
            ) : (
              <Text>No Patients</Text>
            )} */}
            {this.state.Released.length > 0 ? (
              <ImageBackground
                style={{ width: "100%", height: "100%" }}
                source={require("../images/Asset1.png")}
              >
                <Content>
                  <List>
                    {this.state.Released.map((Patient, index) => {
                      return (
                        <ListItem thumbnail style={styles.itemContain}>
                          <Left>
                            {/* source={{ uri: "../images/Asset1.png" }} */}
                            <Thumbnail
                              circle
                              source={require("../images/Asset1.png")}
                            />
                          </Left>
                          <Body>
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate(
                                  "PatientDetails",
                                  {
                                    userId: Patient.PatientId,
                                    userName: Patient.PatientName,
                                    fatherName: Patient.FathersName,
                                    motherName: Patient.MothersName,
                                    mobileNumber: Patient.MobileNumber,
                                    EmergencyContactName:
                                      Patient.EmergencyContactName,
                                    EmergencyMobileNumber:
                                      Patient.EmergencyMobileNumber,
                                    Address: Patient.Address,
                                    Age: Patient.Age,
                                    BloodGroup: Patient.BloodGroup,
                                    Date: Patient.Date
                                  }
                                )
                              }
                            >
                              <Text>{Patient.PatientId}</Text>
                              <Text>{Patient.PatientName}</Text>
                              <Text>Father Name: {Patient.FathersName}</Text>
                            </TouchableOpacity>
                          </Body>
                        </ListItem>
                      );
                    })}
                  </List>
                </Content>
              </ImageBackground>
            ) : (
              <Text>No Patients</Text>
            )}
          </Tab>
          <Tab
            heading="Dead"
            tabStyle={{ backgroundColor: "#046DAD" }}
            textStyle={{ color: "#fff" }}
            activeTabStyle={{ backgroundColor: "#046DAD" }}
            activeTextStyle={{ color: "#fff", fontWeight: "bold" }}
          >
            {/* <DeadList></DeadList> */}
            {this.state.Dead.length > 0 ? (
              <ImageBackground
                style={{ width: "100%", height: "100%" }}
                source={require("../images/Asset1.png")}
              >
                <Content>
                  <List>
                    {this.state.Dead.map((Patient, index) => {
                      return (
                        <ListItem thumbnail style={styles.itemContain}>
                          <Left>
                            {/* source={{ uri: "../images/Asset1.png" }} */}
                            <Thumbnail
                              circle
                              source={require("../images/Asset1.png")}
                            />
                          </Left>
                          <Body>
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate(
                                  "PatientDetails",
                                  {
                                    userId: Patient.PatientId,
                                    userName: Patient.PatientName,
                                    fatherName: Patient.FathersName,
                                    motherName: Patient.MothersName,
                                    mobileNumber: Patient.MobileNumber,
                                    EmergencyContactName:
                                      Patient.EmergencyContactName,
                                    EmergencyMobileNumber:
                                      Patient.EmergencyMobileNumber,
                                    Address: Patient.Address,
                                    Age: Patient.Age,
                                    BloodGroup: Patient.BloodGroup,
                                    Date: Patient.Date
                                  }
                                )
                              }
                            >
                              <Text>{Patient.PatientId}</Text>
                              <Text>{Patient.PatientName}</Text>
                              <Text>Father Name: {Patient.FathersName}</Text>
                            </TouchableOpacity>
                          </Body>
                        </ListItem>
                      );
                    })}
                  </List>
                </Content>
              </ImageBackground>
            ) : (
              <Text>No Patients</Text>
            )}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = {
  itemContain: {
    backgroundColor: "#fff",
    marginTop: 10,
    marginRight: 10,
    paddingLeft: 8,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    opacity: 0.95
  }
};
