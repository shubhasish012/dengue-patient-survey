import React, { Component, Fragment } from "react";
import { Input, Icon, Button } from "react-native-elements";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Alert,
  ImageBackground
} from "react-native";
import HeaderComponent from "../components/HeaderComponent";
export default class HospitalDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitted: false
    };
  }
  static navigationOptions = {
    header: null
  };

  render() {
    const { navigation } = this.props;
    const HospitalName = navigation.getParam("HospitalName");
    const Licensenumber = navigation.getParam("Licensenumber");
    const Address = navigation.getParam("Address");
    const Email = navigation.getParam("Email");
    const Existing = navigation.getParam("Existing");
    const Released = navigation.getParam("Released");
    const Dead = navigation.getParam("Dead");
    return (
      <View>
        <HeaderComponent
          title="Hospital Details"
          leftButton={() => this.props.navigation.openDrawer()}
          iconTitle="menu"
        />
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={require("../images/Asset2.png")}
        >
          <ScrollView>
            <View style={styles.body}>
              <Text style={styles.title}>{HospitalName} information</Text>

              <Text style={styles.text}>Hospital Name: {HospitalName}</Text>
              <Text style={styles.text}>License Number: {Licensenumber}</Text>
              <Text style={styles.text}>Adderess: {Address}</Text>
              <Text style={styles.text}>Email: {Email}</Text>
              <Text style={styles.text}>Existing Patient: {Existing}</Text>
              <Text style={styles.text}>Released Patient: {Released}</Text>
              <Text style={styles.text}>Dead Patient: {Dead}</Text>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    color: "#00789F",
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 30
  },
  body: {
    alignItems: "center",
    backgroundColor: "#FFFFFF90",
    opacity: 50,
    margin: 15,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  text: {
    fontSize: 20,
    color: "#00789F",
    margin: 10,
    fontFamily: "Cochin",
    fontStyle: "italic",
    fontWeight: "bold"
  }
});
