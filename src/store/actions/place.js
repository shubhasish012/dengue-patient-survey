import { ADD_PLACE } from "./types";

export const addPlace = email => {
  return {
    type: ADD_PLACE,
    payload: email
  };
};
